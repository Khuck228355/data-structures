from random import *
ln1=0
ln2=0
while ln1==0 and ln2==0:
    try:
        ln1=int(input("Введите длину 1 списка "))
        ln2=int(input("Введите длину 2 списка "))
        if ln1<0 or ln2<0:
            ln1=0
            ln2=0
            print('Введите положительные числа')
    except ValueError:
        print("Числа не целые")
sp1=list(range(ln1))
sp2=list(range(ln2))
sp3=[]
for i in range(ln1):
    sp1[i]=randint(-50, 50)
for i in range(ln2):
    sp2[i] = randint(-50, 50)
print(sp1)
print(sp2)
for i in range(ln1):
    if sp1[i]>=0:
        sp3.append(sp1[i])
    else:
        sp3.append(1)
for i in range(ln2):
    if sp2[i]>=0:
        sp3.append(sp2[i])
    else:
        sp3.append(1)
print(sp3)