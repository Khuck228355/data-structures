from random import *
ln1=0
ln2=0
while ln1==0 and ln2==0:
    try:
        ln1=int(input('Кол-во строк = '))
        ln2=int(input('Кол-во столбиков = '))
        k=int(input('Кол-во единиц = '))
        if ln1<0 or ln2<0:
            ln1=0
            ln2=0
            print('Введите положительные числа')
        if k<=0:
            k=0
            print('Число должно быть больше 0')
    except ValueError:
        print("Числа не целые")
c=k
lst=[[0 for i in(range(ln2))] for i in(range(ln1))]
while c!=0:
    xt=randint(0, ln1-1)
    yt=randint(0, ln2-1)
    if lst[xt][yt]==0:
        lst[xt][yt]=1
        c-=1
for i in range(ln1):
    print(lst[i])

print('------------------------------------')
for i in range(ln1):
    for i1 in range(ln2):
        if lst[i][i1]==0:
            lst[i][i1]=' '
        if lst[i][i1]==1:
            lst[i][i1]='*'
for i in range(ln1):
    print(lst[i])
for i in range(ln1):
    print(lst[i], lst[i][::-1])
for i in range(ln1, 0, -1):
    print(lst[i-1], lst[i-1][::-1])
