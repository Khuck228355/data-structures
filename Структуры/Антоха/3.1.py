import matplotlib.pyplot as plt
from random import *
from datetime import *
from time import *
from sys import *
def maxmin(lst, stind):
    m1=stind
    for i in range(stind, len(lst)):
        if lst[i]<lst[m1]:
            m1=i
    return m1
def BubbleSort(sp):
    not_sorted=True
    while not_sorted:
        flag=False
        for i in range(len(sp)):
            try:
                if sp[i]>sp[i+1]:
                    flag = True
                    sp[i], sp[i+1] = sp[i+1], sp[i]
            except:
                continue
        not_sorted=flag
    return sp
def VklSort(sp, graph):
    not_sorted = True
    while not_sorted:
        flag = False
        for i in range(len(sp)):
            nottrueplace=True
            while nottrueplace:
                if graph:
                    sleep(2)
                    stdout.write('\r'+str(sp))
                flag=False
                try:
                    if sp[i] > sp[i + 1]:
                        flag = True
                        sp[i], sp[i + 1] = sp[i + 1], sp[i]
                        i-=1
                        if i<0:
                            nottrueplace=False
                            continue
                except:
                    nottrueplace = flag
                    continue
                nottrueplace = flag
        not_sorted = flag
    return sp
def ChooseSort(sp):
    for i in range(len(sp)):
        mn=maxmin(sp, i)
        sp[i], sp[mn] = sp[mn], sp[i]
    return sp
def ShakerSort(sp):
    not_sorted=True
    left=0
    right=len(sp)-1
    while not_sorted:
        flag=False
        for i in range(left, right, +1):
            if sp[i] > sp[i + 1]:
                flag=True
                sp[i], sp[i + 1] = sp[i + 1], sp[i]
        right -= 1
        for i in range(right, left, -1):
            if sp[i - 1] > sp[i]:
                flag = True
                sp[i], sp[i - 1] = sp[i - 1], sp[i]
        left += 1
        not_sorted=flag
    return sp
def Shellsort(sp):
    lst = len(sp)
    half = len(sp)//2
    while half > 0:
        for i in range(half, lst, 1):
            p = i
            temp = p - half
            while temp >= 0 and sp[temp] > sp[p]:
                sp[temp], sp[p] = sp[p], sp[temp]
                p = temp
                temp = p - half
        half //= 2
    return sp
def HoarSort(sp):
    if len(sp) <= 1:
        return sp
    else:
        q = choice(sp)
        s_sp = []
        m_sp = []
        e_sp = []
        for i in sp:
            if i < q:
                s_sp.append(i)
            elif i > q:
                m_sp.append(i)
            else:
                e_sp.append(i)
        return HoarSort(s_sp) + e_sp + HoarSort(m_sp)
x=0
while x==0:
    try:
        x=int(input('Введите длину списка: '))
        if x<0:
            x=0
            print('Введите положительное число')
    except ValueError:
        print("Число не целое")
sp1=list(range(x))
for i in range(x):
    sp1[i]=randint(0, 100)
timebefore=datetime.now()
BubbleSort(sp1)
timeafter=datetime.now()
timebubblesort=timeafter-timebefore
print(timebubblesort)
sp1=list(range(x))
for i in range(x):
    sp1[i]=randint(0, 100)
timebefore = datetime.now()
VklSort(sp1, False)
timeafter=datetime.now()
timevklsort=timeafter-timebefore
print(timevklsort)
for i in range(x):
    sp1[i]=randint(0, 100)
timebefore=datetime.now()
ChooseSort(sp1)
timeafter=datetime.now()
timechoosesort=timeafter-timebefore
print(timechoosesort)
for i in range(x):
    sp1[i]=randint(0, 100)
timebefore=datetime.now()
ShakerSort(sp1)
timeafter=datetime.now()
timeShakersort=timeafter-timebefore
print(timeShakersort)
for i in range(x):
    sp1[i]=randint(0, 100)
timebefore=datetime.now()
Shellsort(sp1)
timeafter=datetime.now()
timeShellsort=timeafter-timebefore
print(timeShellsort)
for i in range(x):
    sp1[i]=randint(0, 100)
timebefore=datetime.now()
HoarSort(sp1)
timeafter=datetime.now()
timeHoarsort=timeafter-timebefore
print(timeHoarsort)
plt.bar(1, int((timebubblesort.microseconds)), 1)
plt.bar(2, int((timevklsort.microseconds)), 1)
plt.bar(3, int((timechoosesort.microseconds)), 1)
plt.bar(4, int((timeShakersort.microseconds)), 1)
plt.bar(5, int((timeShellsort.microseconds)), 1)
plt.bar(6, int((timeHoarsort.microseconds)), 1)
plt.show()