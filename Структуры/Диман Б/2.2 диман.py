from math import *
from random import *
ln=0
while ln==0:
    try:
        ln=int(input("Введите длину списка"))
        if ln<0:
            ln=0
            print('Введите положительное число')
    except ValueError:
        print("Число не целое")
sp1=list(range(ln))
sp2=list(range(ln))
for i in range(ln):
    sp1[i]=randint(0, 50)
    sp2[i] = randint(0, 50)
print(sp1)
print(sp2)
for i in range(ln):
    x=sp1[i]
    y=sp2[i]
    z=sqrt((x+y)/2)
    print(i, ': ', 'sqrt((', x, '+', y, ')/2 = ', z, sep='')