import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import datetime as dt


def hg(ct, pt):
    if ct > 20:
        return True
    else:
        return False


def loading(loadname):
    # Функция для отображения текущей задачи в реальном времени
    if loadname == 1:
        sys.stdout.write('Вывод директории\n')
    if loadname == 2:
        sys.stdout.write('\rДобавление периодов в список')
    if loadname == 3:
        sys.stdout.write('\rОбединение таблиц с проходами')
    if loadname == 4:
        sys.stdout.write('\rОтсеивание таблицы')
    if loadname == 5:
        sys.stdout.write('\rПреобразование данных в числа')
    if loadname == 6:
        sys.stdout.write('\rСоздание столбца температуры')
    if loadname == 7:
        sys.stdout.write('\rПоиск проходов и заполнение конечной таблицы')


yn = 'y'
direct2 = os.listdir(path="Results")
profiles = list()
profiles = list(set(profiles))
# Глобальный цикл, перезапускающийся, пока пользователь не введет 'n' после выполнения
loading(1)
# Создание массивов с директориями начальной и конечной папки
# Список профилей задается вручную. Profileslist - список, содержащий все периоды по данному профилю
profileslist = list()
# Вывод файлов в директории, чтобы пользователь легко мог выбрать файл, который ему нужно обработать
df1 = pd.DataFrame({})
loading(2)
# Добавление всех периодов по заданному профилю в один список
df2 = pd.DataFrame({})
# Цикл обработки периодов по заданному профилю
err = 0
# Добавление файла в датафрейм
df1 = pd.read_csv('debug.txt', sep='	')
print(df1)
globalcount = -1
# Переименование столбцов
loading(4)
#Отсеивание таблиц от 'мусорных' данных
for i in range(len(df1.columns)):
    df1 = df1.loc[(df1[df1.columns[i]] != '1,#INF') &
                (df1[df1.columns[i]].notna()) &
                (df1[df1.columns[i]] != '-1,#IND') &
                (df1[df1.columns[i]] != 'nan')&(df1[df1.columns[i]] != '1.#INF')]
#Задание списков с данными из столбцов
timelist = list(df1['Time'])
amplist = list(df1['Amperage'])
powlist = list(df1['Power'])
speedlist = list(df1['Speed'])
temprlist = list(df1['Tstand'])
c4list = list(df1['Controler4'])
c5list = list(df1['Controler5'])
k = 0
# Цикл преобразования данных в числа
for i in range(len(amplist)):
    amplist[i] = amplist[i].replace(',', '.')
    amplist[i] = float(amplist[i])
    powlist[i] = powlist[i].replace(',', '.')
    powlist[i] = float(powlist[i])
    speedlist[i] = speedlist[i].replace(',', '.')
    speedlist[i] = float(speedlist[i])
    temprlist[i] = temprlist[i].replace(',', '.')
    temprlist[i] = float(temprlist[i])
    c4list[i] = int(c4list[i])
    c5list[i] = int(c5list[i])
#Удаление данных 5 клети, если они попадаются как первый проход
if float(amplist[0])>20:
    while int(c5list[0])==0:
        k += 1
        amplist.pop(0)
        powlist.pop(0)
        speedlist.pop(0)
        temprlist.pop(0)
        c4list.pop(0)
        c5list.pop(0)
        timelist.pop(0)
if int(c4list[0]) == 0:
    while int(c4list[0]) == 0:
        k += 1
        amplist.pop(0)
        powlist.pop(0)
        speedlist.pop(0)
        temprlist.pop(0)
        c4list.pop(0)
        c5list.pop(0)
        timelist.pop(0)
#Объявление переменных, списков и меток(флагов)
prevtn = 5
localcount = -1
numstand = list()
ft = 1
instand = list()
counttimes = 0
flagcountstarted=0
flagcountstarted2=0
amplisttemp=list()
powlisttemp=list()
speedlisttemp=list()
temprlisttemp=list()
iprev=0
sdstand=0
globalcountprev=0
lableiprev=1
lablenullstand=1
instandlable=1
instandtemp=list()
numstandtemp=list()
counttimeslist=list()
evercount=0
evercountlable=0
loading(5)
powmax=0
ttmx=0
ttmn=0
preved=0
loading(7)
#Цикл для нахождения проходов, и добавления их в конечный файл. За динамическую переменную берется текущее значение тока.
#В этом цикле при определенных условиях могут менятся значения меток(флагов)
for i in range(len(amplist)):
    globalcount = i
    #Если в клети нету заготовки, то на графике номер клети равен 0
    if lablenullstand==1:
        numstand.append(0)
    if instandlable == 1:
        instand.append(0)
    # Измерение данных температуры
    if temprlist[i] > 0:
        temprlisttemp.append(temprlist[i])
        flagcountstarted2=1
    if flagcountstarted2 == 1:
        try:
            if temprlist[i] == 0 and temprlist[i+1]==0 and temprlist[i+2]==0:
                ttmx = max(temprlisttemp)
                ttmn = np.mean(temprlisttemp)
                df2.loc[len(df2['TempMax'])-1, 'TempMax'] = ttmx
                df2.loc[len(df2['TempMean'])-1, 'TempMean'] = ttmn
                flagcountstarted2=0
                temprlisttemp.clear()
        except IndexError:
            if temprlist[i] == 0:
                ttmx = max(temprlisttemp)
                ttmn = np.mean(temprlisttemp)
                df2.loc[len(df2['TempMax'])-1, 'TempMax'] = ttmx
                df2.loc[len(df2['TempMean']) - 1, 'TempMean'] = ttmn
                flagcountstarted2 = 0
                temprlisttemp.clear()
    #Проверка нахождения заготовки в клети через разницу между текущим и предыдущим значениями тока.
    if amplist[globalcount]-iprev>10: #Заготовка в клети
        lableiprev=0
        #Здесь переменной sd задается время начала только в первый проход цикла
        if ft==1:
            sd = dt.datetime.strptime(timelist[globalcount], "%d.%m.%Y %H:%M:%S.%f")
            sdstand = globalcount
            #Вычисление паузы между проходами, используя дату начала и предыдущую дату окончания прохода.
            if type(preved)==int:
                pause = 'null'
            else:
                pause = str(sd-preved)
            ft=0
        #Переменная evercount нужна для вывода графика начиная с момента первого прохода. Задается только один раз за весь главный цикл
        if evercountlable == 0:
            evercount = globalcount
            evercountlable = 1
        counttimes+=1
        flagcountstarted=1
        #Добавление текущих значений параметров во временные списки
        amplisttemp.append(amplist[globalcount])
        powlisttemp.append(powlist[globalcount])
        speedlisttemp.append(speedlist[globalcount])
        if c4list[globalcount] == 1:
            numstandtemp.append(4)
        if c5list[globalcount]==1:
            numstandtemp.append(5)
        lablenullstand=0
        instandtemp.append(1)
        instandlable=0
        counttimeslist.append(counttimes)
    if amplist[globalcount]-iprev<=10 and flagcountstarted==1: #Итоговый подсчёт
        #Сброс значений меток
        lableiprev=1
        lablenullstand=1
        instandlable=1
        flagcountstarted=0
        ft=1
        if powlisttemp!=[]:
            powmax = max(powlisttemp)
        else:
            powmax = 0
        #Добавление данных в итоговую таблицу, только если время подьема значения тока больше определенного.
        if hg(counttimes, powmax):
            counttimes=0
            if c4list[sdstand]==1:
                tn=4
            if c5list[sdstand]==1:
                tn=5
                localcount += 1
            #Вычисление всех необходимых параметров, исходя из данных во временных списках
            ed=dt.datetime.strptime(timelist[globalcount-1], "%d.%m.%Y %H:%M:%S.%f")
            numstand+=numstandtemp
            instand+=instandtemp
            tamx=max(amplisttemp)
            tamn=np.mean(amplisttemp)
            tpmx=max(powlisttemp)
            tpmn=np.mean(powlisttemp)
            tsmx=max(speedlisttemp)
            #Добавление данных в таблицу, толко если вместе с током поднимается мощность
            if prevtn==tn:
                err+=1
            else:
                newrow = pd.DataFrame(
                    {'Startdate': [sd], 'Enddate': [ed], 'Pause': [pause], 'NumStand': [tn], 'AmpMax': [tamx],
                     'AmpMean': [tamn], \
                     'PowMax': [tpmx], 'PowMean': [tpmn], 'SpeedMax': [tsmx], 'TempMax': [ttmx],
                     'TempMean': [ttmn]})
                df2 = pd.concat([df2, newrow], ignore_index=True)
            ttmx=0
            ttmn=0
            prevtn = tn
            preved = ed
            #Очищение временных списков
            amplisttemp.clear()
            powlisttemp.clear()
            speedlisttemp.clear()
            numstandtemp.clear()
            instandtemp.clear()
        else:
            counttimes=0
            amplisttemp.clear()
            powlisttemp.clear()
            speedlisttemp.clear()
            numstandtemp.clear()
            instandtemp.clear()
    if lableiprev==1:
        iprev=amplist[globalcount]
    globalcountprev=globalcount
print(df2)
#Удаление последней строки из таблицы, если в ней данные по проходу на 4 клети
numlist=list(df2['NumStand'])
lastnumber=len(numlist)-1
if numlist[lastnumber]==4:
    df2 = df2.drop(labels=[lastnumber], axis=0)
print('\nОшибочно обработаных проходов:', err)
if ((err / len(amplist)) * 100) <= 5:
    df2.to_csv(r'Results\.txt')
numlist=list(df2['NumStand'])
print(numlist.count(4), numlist.count(5))
#Вывод графика (В данный момент отключен)
yn='y'
if yn=='y':
    low_ground = 0
    high_ground = len(amplist)
    fig, ax = plt.subplots(3,1,figsize=(16,15))
    ax[0].plot(amplist[low_ground:high_ground])
    ax[0].set_title("Ток")
    ax[1].plot(numstand[low_ground:high_ground])
    ax[1].set_title("Номер клети")
    ax[2].plot(instand[low_ground:high_ground])
    ax[2].set_title("Флаг наличия заготовки в клети")
    plt.show()
