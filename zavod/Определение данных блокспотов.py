import pandas as pd
import numpy as np
import os
yn = 'y'
# Глобальный цикл, перезапускающийся, пока пользователь не введет 'n' после выполнения
while yn == 'y':
    # Создание массивов с директориями начальной и конечной папки
    direct1 = os.listdir(path="C:/Users/dedigarev/PycharmProjects/om1/datapreprocessing_aatp/45stand/Results")
    direct2 = os.listdir(path="C:/Users/dedigarev/PycharmProjects/om1/datapreprocessing_aatp/45stand/ResultAnalys")
    # Вывод файлов в директории, чтобы пользователь легко мог выбрать файл, который ему нужно обработать
    for i in range(len(direct1)):
        if 'analys' + direct1[i] in direct2:
            print(i, direct1[i])
        else:
            print(i, direct1[i], 'Не обработан')
    print('Выберите номер файла, для которого хотите произвести обработку данных, либо all для всех файлов')
    filenum = input()
    # Цикл, обрабатывающий все файлы если пользователь ввел 'all', либо проходящий всего 1 раз
    for globalcycle in direct1:
        # Определение имени обрабатываемого файла/файлов из массива с именами файлов
        if filenum != 'all':
            filenum = int(filenum)
            filename = direct1[filenum]
        else:
            filename = globalcycle
        df = pd.read_csv('C:/Users/dedigarev/PycharmProjects/om1/datapreprocessing_aatp/45stand/Results/' + filename)
        listpar = ['AmpMax', 'AmpMean', 'PowMax', 'PowMean', 'SpeedMax', 'TempMax', 'TempMean']
        # Создание таблицы через стандартную функцию open
        f = open("C:/Users/dedigarev/PycharmProjects/om1/datapreprocessing_aatp/45stand/ResultAnalys/analys" + filename, "w")
        f.write('NumStand,q1,mid,q3,x1,x2,par\n')
        # Цикл для добавления информации в таблицу построчно
        for par in range(len(listpar)):
            for i in range(4, 6):
                meanfile = []
                temparr = df[(df['NumStand'] == i) & (df[f'{listpar[par]}'] > 0)][f'{listpar[par]}']
                if len(temparr) > 0:
                    q1 = float(np.quantile(temparr, 0.25))
                    q3 = float(np.quantile(temparr, 0.75))
                    iqr = 1.5 * (q3 - q1)
                    minn = float(np.min(temparr))
                    maxx = float(np.max(temparr))
                    mid = float(np.median(temparr))
                    x1 = q1 - iqr if q1 - iqr > minn else minn
                    x2 = q3 + iqr if q3 + iqr < maxx else maxx
                    newrow = [i, q1, mid, q3, x1, x2, listpar[par]]
                else:
                    newrow = [0, 0, 0, 0, 0, 0, listpar[par]]
                meanfile.append(newrow)
                for word in range(len(newrow)):
                    f.write(str(newrow[word]))
                    # Условие ниже ставит запятую после параметра в таблице если он не последний
                    if word+1 != len(newrow):
                        f.write(',')
                f.write("\n")
        print('\nФайл', filename, 'обработан.')
        if filenum != 'all':
            break
    print('Продолжить? y/n')

    yn = input()