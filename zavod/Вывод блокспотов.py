import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import time
import sys
import threading
yn='y'
#Глобальный цикл, перезапускающийся, пока пользователь не введет 'n' после выполнения
while yn=='y':
    k=0
    # Создание массивов с директорией папки
    direct1 = os.listdir(path="C:/Users/dedigarev/PycharmProjects/om1/datapreprocessing_aatp/45stand/ResultAnalys")
    # Вывод файлов в директории, чтобы пользователь легко мог выбрать файл, который ему нужно обработать
    for i in range(len(direct1)):
        print(i, direct1[i])
    print('Выберите файл, для которого хотите произвести обработку данных.')
    filename = int(input())
    # Чтение таблицы с данными для построения блокспотов
    mypar = pd.read_csv('C:/Users/dedigarev/PycharmProjects/om1/datapreprocessing_aatp/45stand/ResultAnalys/'+direct1[filename])
    params=list(mypar['par'])
    # Удаление повторяющихся имен параметров в их списке
    for i in range(len(params)//2):
        params.pop(i)
    # Создание блокспотов, исходя из данных
    for par in params:
        stats = []
        for i in range(2):
            q1 = float(mypar['q1'][i+k])
            mid = float(mypar['mid'][i+k])
            q3 = float(mypar['q3'][i+k])
            x1 = float(mypar['x1'][i+k])
            x2 = float(mypar['x2'][i+k])
            stats.append({'med': mid,
                          'q1': q1,
                          'q3': q3,
                          'whislo': x1,
                          'whishi': x2
                          })
        fig, ax = plt.subplots(figsize=(16, 7))
        b = ax.bxp(stats, showfliers=False, patch_artist=True)
        ax.grid(True)
        for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
            plt.setp(b[element], color='blue', linewidth=2)
        for patch in b['boxes']:
            patch.set(facecolor='lime', linewidth=2)
        plt.show()
        k+=2
    print('Файл', filename, 'обработан. Продолжить? y/n')
    yn = input()
