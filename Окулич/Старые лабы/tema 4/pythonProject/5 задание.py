def list_sort(lst):
    lst.sort(key=lambda x: abs(x), reverse=True)
    return lst

# Тесты
print(list_sort([1, 2, 3]))
print(list_sort([1, 3, -4, 11, 0, 7]))
print(list_sort([33, -0.05, -4.18, 11.2, 13.12, 55]))