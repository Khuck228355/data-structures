def useless(lst):
    return max(lst) / len(lst)

# Тесты
print(useless([1, 2, 3]))
print(useless([1, 3, -4, 11, 0, 7]))
print(useless([33, -0.05, -4.18, 11.2, 13.12, 55]))