class Личность:
    def __init__(self, имя=None):
        self.__имя = имя

    @property
    def имя(self):
        return self.__имя

    @имя.setter
    def name(self, новое_имя):
        self.__имя = новое_имя

человек_1 = Личность('Иван')
print('14', человек_1.имя)
человек_1.name = 'Василий'
print('16:', человек_1.имя)
человек_1 = Личность('Иван')
print('18:', человек_1.имя)
человек_1.nane = 'Василий'
print('20:', человек_1.имя)
