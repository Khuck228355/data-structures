class Работник:
    id = -1
    surname =''
    def __init__ (self, i, n, r):
        self.id = i
        self.surname = n
        self.role = r

    def __str__(self):
        return 'Работник [id = {}, должность={}'.format(self.id, self.surname, self.role)
раб_1 = Работник(123, 'Петров', 'слесарь')
print(раб_1)
print('Печать отдельных переменных')
print('id =',раб_1.id)
print('фамилия =', раб_1.surname)
print('должность =', раб_1.role)
print('Удаляем поочерёдно атрибуты класса, используя оператор del, и смотрим результат' )
del раб_1.id
del раб_1.surname
print(раб_1) # При удалении атрибутов класса у экземпляра устанавливаются значения, заданные в классе
print('Удаляем используя оператор del переменную (атрибут) объекта, и смотрим результат')
del раб_1.role
print(раб_1) # после удаления атрибута экземпляра система не обнаруживает наличия такого атрибута и останавливается
раб_2 = Работник(333, 'Иванов','директор')
print('167:', раб_2)
delattr(рaб_2, 'id')
delattr(рa6_2, 'surname')
delattr(раб_2, 'role')
print('171:', раб_2)