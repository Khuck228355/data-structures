import random
import sys

f = int(input("Введите массив : "))
h = []
for i in range(f):
    h.append(random.random())

print("Исходный массив", h)

minimum = sys.maxsize
for i in h:
    if (minimum > i and i >= 0):
        minimum = i
print("Наименьшый положительный элемент = ", minimum)