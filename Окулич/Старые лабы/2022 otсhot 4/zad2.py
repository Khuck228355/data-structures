class Class1:  # Базовый класс
    def __init__(self):
        print("Конструктор базового класса")

    def func(self):
        print("Метод func() класса Class1")


class Class2(Class1):  # Класс Class2 наследует класс Class1
    def __init__(self):
        print("Конструктор производного класса")
        Class1.__init__(self)  # Вызываем конструктор базового класса

    def func(self):
        print("Метод func() класса Class2")
        Class1.func(self)  # Вызываем метод базового класса


c1 = Class2()  # Создаем экземпляр класса Class2
c1.func()  # Вызываем метод func()
