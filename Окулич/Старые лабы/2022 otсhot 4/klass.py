class Automobile:
    def __init__ (self, make, model, mileage, price):
        self.__make = make
        self.__model = model
        self. __mileage = mileage
        self.__price = price

    # Ниже - методы сеттеры (модификаторы) базового класса
    def set_make(self, make):
        self.__make = make

    def set_model(self, model):
        self.__model = model

    def set_mileage(self, mileage):
        self.__mileage = mileage

    def set_price(self, price):
        self.__price = price


    # Следующие методы позволяют узнать (получить) атрибуты класса
    def get_make(self):
        return self.__make

    def get_model(self):
        return self.__model

    def get_mileage(self):
        return self.__mileage

    def get_price(self):
        return self.__price


class Car(Automobile): # Создание подкласса (дочернего класса)
    def __init__(self, make, model, mileage, price, kol_dver):
        Automobile. __init__ (self, make, model, mileage, price)
        self.__kol_dver = kol_dver
    def set_kol_dver(self, kol_dver):
        self.__kol_dver = kol_dver
    def get_kol_dver(self):
        return self.__kol_dver

#
class Picap(Automobile): # Создание подкласса (дочернего класса)
    def __init__(self, make, model, mileage, price, kol_dver, korobka):
        Automobile. __init__ (self, make, model, mileage, price)
        self.__kol_dver = kol_dver
        self.__korobka = korobka
    def set_kol_dver(self, kol_dver):
        self.__kol_dver = kol_dver
    def get_kol_dver(self):
        return self.__kol_dver
    def set_korobka(self, korobka):
        self.__korobka = korobka
    def get_korobka(self):
        return self.__korobka

class Gipe(Automobile): # Создание подкласса (дочернего класса)
    def __init__(self, make, model, mileage, price, kol_dver, korobka):
        Automobile. __init__ (self, make, model, mileage, price)
        self.__kol_dver = kol_dver
        self.__korobka = korobka
    def set_kol_dver(self, kol_dver):
        self.__kol_dver = kol_dver
    def get_kol_dver(self):
        return self.__kol_dver
    def set_korobka(self, korobka):
        self.__korobka = korobka
    def get_korobka(self):
        return self.__korobka
