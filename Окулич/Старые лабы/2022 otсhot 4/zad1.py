import klass

авто_1 = klass.Gipe('BMV', 15, 150000, 1500000.0, 5, 'nz2')
авто_2 = klass.Picap('Ford', 'Ranger', 50000, 3000000.0, 4, 'vg-351')


print("Для Джипа авто: ")
print('Изготовитель:', авто_1.get_make())
print('Модель:', авто_1.get_model())
print('Пробег:', авто_1.get_mileage())
print('Цена:', авто_1.get_price())
print('Дверей:', авто_1.get_kol_dver())
print('Коробка:',авто_1.get_korobka())
print('#################################')
print("Для Пикапа: ")
print('Изготовитель:', авто_2.get_make())
print('Модель:', авто_2.get_model())
print('Пробег:', авто_2.get_mileage())
print('Цена:', авто_2.get_price())
print('Дверей:', авто_2.get_kol_dver())
print('Коробка:',авто_2.get_korobka())
