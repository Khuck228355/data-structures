# Мномественное наследование
class Class1: # Базовый класс для класса Class2
    def funcl(self):
        print("Maron func1() класса Closx1")
class Class2(Class1): #Класс Class2 наследует класс Classi
    def func2(self):
        print("Метод func2() класса Class2")
class Class3(Class1): # Класс Class3 наследует класс Classi
    def func1(self):
        print("Метод func1() класса Class3")
    def func2(self):
        print("Метод fanc2() класся Class3")
    def func3(self):
        print("Метод func3() кпассa Class3")
    def func4(self):
        func2=Class3.func2
        print( "Метод func4() классa Class3")
class Class4(Class2, Class3): # Аножественное наследование
    def func4(self):
        print("Mетoд func4() класса Class4")
class Naslednic(Class4):
    def naslednic(self):
        print('Наследник')
c = Naslednic() # Создвем экземплар класса Class4
c.func1() # Вывод: Нетод funcl() класса Class3
c.func2() # Bывод: Метод tunc2() класса Class2
c.func3() # Вывод: Метод func3() класса Class3
c.func4() # Вывод: Метод func4() класса Class4
c.naslednic()
