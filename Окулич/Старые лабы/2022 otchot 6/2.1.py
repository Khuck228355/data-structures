from tkinter import *

окно1 = Tk()
w = 250
h = 250
ws = окно1.winfo_screenwidth()
hs = окно1.winfo_screenheight()
x = (ws/2) - (w/2)
y = (hs/2) - (h/2)
окно1.geometry('%dx%d+%d+%d' % (w, h, x, y))
окно1.title("Главное окно!")
def close():
    окно1.destroy()
# def click1():
def click1(event):
    окно2 = Tk()
    окно2.title("Совсем новое окно")
    w = 250
    h = 250
    ws = окно2.winfo_screenwidth()
    hs = окно2.winfo_screenheight()
    x = (ws / 2) - (w / 2) + 260
    y = (hs / 2) - (h / 2)
    окно2.geometry('%dx%d+%d+%d' % (w, h, x, y))
    кнопка2 = Button(окно2, text="Кнопка без событий и действий", command=close)
    кнопка2.pack(anchor=CENTER, expand=1)

# кнопка1 = Button(text="Создать окно", command=click1)
кнопка1 = Button(text="Создать окно")
кнопка1.bind('<Button-1>', click1)
кнопка1.pack(anchor=CENTER, expand=1)
окно1.mainloop()
