class ParentClass:
    def __init__(self, var):
        self.var=var
    def __str__(self):
        return self.var
    def fact(self):
        return self.var
class ChildClass1(ParentClass):
    def __init__(self, avar):
        super().__init__(avar)
    def fact(self):
        return self.var
class ChildClass2(ParentClass):
    def __init__(self, avar):
        super().__init__(avar)
    def area(self):
        return(32)
b=ChildClass2('hi')
a=ChildClass1(43)
print(b)
print(b.fact())
print(a.fact())
print(b.area())