class Class1:
    def __init__(self):
        print('41: Сработал конструктор базового класса Class1')
    def func1(self):
        print('43: Сработал метод func1() экземпляра класса Class1')

class Class2(Class1):
    def __init__(self):
        print('47:', 'Сработал конструктор производного класса')

    def func1(self):
        print('50:', 'Работает метод func1() экземпляра класса Class2')
        Class1.func1(self)
        Class1.__init__(self)

print('53: Переход к создание объекта Class1')
obj_class1 = Class1()
obj_class1.func1()

print('57: Переход к созданию объекта Class2')
obj_class2 = Class2()
obj_class2.func1()
