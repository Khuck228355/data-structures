class Automobile:
    def __init__(self, make, model, mileage, price):
        self.__make=make
        self.__model=model
        self.__mileage=mileage
        self.__price=price

    def set_make(self, make):
        self.__make=make

    def set_model(self, model):
        self.__model = model

    def set_model(self, mileage):
        self.__mileage = mileage

    def set_price(self, price):
        self.__price=price

    def get_make(self):
        return self.__make

    def get_model(self):
        return self.__model

    def get_mileage(self):
        return self.__mileage

    def get_price(self):
        return self.__price


class Car(Automobile):
    def __init__(self, make, model, mileage, price, kol_dver, sold):
        Automobile.__init__(self, make, model, mileage, price)
        self.__kol_dver = kol_dver
        self.__sold = sold
    def set_kol_dver(self, kol_dver):
        self.__kol_dver = kol_dver
    def get_kol_dver(self):
        return self.__kol_dver
    def set_sold(self, sold):
        self.__sold = sold
    def get_sold(self):
        return self.__sold
class pickup(Automobile):
    def __init__(self, make, model, mileage, price, kol_dver, marka_dvigatela, sold):
        Automobile.__init__(self, make, model, mileage, price)
        self.__kol_dver = kol_dver
        self.__marka_dvigatela = marka_dvigatela
        self.__sold = sold
    def set_kol_dver(self, kol_dver):
        self.__kol_dver = kol_dver
    def get_kol_dver(self):
        return self.__kol_dver
    def set_marka_dvigatela(self, marka_dvigatela):
        self.__marka_dvigatela = marka_dvigatela
    def get_marka_dvigatela(self):
        return self.__marka_dvigatela
    def set_sold(self, sold):
        self.__sold = sold
    def get_sold(self):
        return self.__sold

class Jip(Automobile):
    def __init__(self, make, model, mileage, price, kol_dver, komplekt, sold):
        Automobile.__init__(self, make, model, mileage, price)
        self.__kol_dver = kol_dver
        self.__komplekt = komplekt
        self.__sold = sold
    def set_kol_dver(self, kol_dver):
        self.__kol_dver = kol_dver

    def get_kol_dver(self):
        return self.__kol_dver

    def set_komplekt(self, komplekt):
        self.__komplekt = komplekt

    def get_komplekt(self):
        return self.__komplekt
    def set_sold(self, sold):
        self.__sold = sold
    def get_sold(self):
        return self.__sold