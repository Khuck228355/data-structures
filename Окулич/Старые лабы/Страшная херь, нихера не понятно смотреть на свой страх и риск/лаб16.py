class Base:
    def __init__(self):
        self.foo='foo'

class Sub(Base):
    def __init__(self):
        super().__init__()

obj = Base()
sub_obj = Sub()
print(isinstance(obj, Base))
# True. Obj - экземпляр класса Base
print(isinstance(obj, Sub))
# False. Obj - не экземпляр класса Sub
print(isinstance(sub_obj, Base))
# True. sub_obj - экземпляр класса Base, потому что класс Sub - дочерний по отношению к классу Base
print(isinstance(sub_obj, Sub))
# True. sub_obj - экземпляр класса Sub
print(type(sub_obj) == Base)
# False. sub_obj - экземпляр класса Base, потому что класс Sub - дочерний по отношению к классу Base
# Однако тип sub_obj - это не класс Base
print(type(sub_obj))
# <class '__main__.Sub'>. Вывод типа экземпляра sub_obj
print(type(sub_obj) == Sub)
# True. sub_obj - экземпляр класса Base, потому что класс Sub - дочерний по отношению к классу Base
# Тип sub_obj - это класс Sub
print(issubclass(type(sub_obj), Base))
# True. тип sub_obj - экземпляр класса Base, потому что класс Sub - дочерний по отношению к классу Base
x = 1
print(isinstance(x, int))
# True. Тип х - int
x = [1, 2, 3]
print(isinstance(x, list))
# True. Тип х - list
x = (1, 2, 3)
print(isinstance(x, tuple))
# True. Тип х - tuple
# Проверим, является ли строка 'Hello' одним из типов, описанных в параметре print(type
print(isinstance('Hello', (float, int, str, list, dict, tuple)))
# True. Тип 'Hello' - str
# Проверка, на принадлежность к экземпляром myObj
class myObj:
  name = "John"
y = myObj()
print(isinstance(y, myObj))
# True. y - экземпляр класса myObj
