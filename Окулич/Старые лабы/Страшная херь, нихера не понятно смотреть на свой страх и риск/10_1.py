from tkinter import *
from tkinter import ttk

def click2(дочерн_окно):
    дочерн_окно.grab_release()
    дочерн_окно.destroy()

def click1():
    дочерн_окно = Toplevel(корневое_окно)
    дочерн_окно.title("Дочернее окно")
    дочерн_окно.geometry('300x200+400+100')
    кнопка_закрыть = ttk.Button(дочерн_окно, text="Закрыть окно", command=lambda: click2(дочерн_окно))
    кнопка_закрыть.pack(anchor="center", expand=1)
    дочерн_окно.focus_set()

корневое_окно = Tk()
корневое_окно.title("Корневое окно")
корневое_окно.geometry("300x200+70+100")
кнопка_создать = Button(корневое_окно, text="Создать дочернее окно", command=click1)
кнопка_создать.pack(anchor="center", expand=1)

корневое_окно.mainloop()
