from tkinter import *
окно1 = Tk()
окно1.title("Главное окно!")
окно1.geometry("250x250")
def close():
    окно1.destroy()
# def click1():
def click1(event):
    окно2 = Tk()
    окно2.title("Совсем новое окно")
    окно2.geometry("200x200")
    кнопка2 = Button(окно2, text="Кнопка без событий и действий", command=close)
    кнопка2.pack(anchor=CENTER, expand=1)

# кнопка1 = Button(text="Создать окно", command=click1)
кнопка1 = Button(text="Создать окно")
кнопка1.bind('<Button-1>', click1)
кнопка1.pack(anchor=CENTER, expand=1)
окно1.mainloop()