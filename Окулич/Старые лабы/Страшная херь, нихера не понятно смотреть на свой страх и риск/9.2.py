from tkinter import *
def calcplus():
    global res
    try:
        x=float(inp1.get())
        y=float(inp2.get())
    except:
        res.pack_forget()
        res = Label(text=('Ошибка'))
        res.pack()
        return
    res.pack_forget()
    res=Label(text=(x+y))
    res.pack()
def calcminus():
    global res
    try:
        x = float(inp1.get())
        y = float(inp2.get())
    except:
        res.pack_forget()
        res = Label(text=('Ошибка'))
        res.pack()
        return
    res.pack_forget()
    res=Label(text=(x-y))
    res.pack()
def calcmn():
    global res
    try:
        x = float(inp1.get())
        y = float(inp2.get())
    except:
        res.pack_forget()
        res = Label(text=('Ошибка'))
        res.pack()
        return
    res.pack_forget()
    res=Label(text=(x*y))
    res.pack()
def calcdel():
    global res
    try:
        x = float(inp1.get())
        y = float(inp2.get())
    except:
        res.pack_forget()
        res = Label(text=('Ошибка'))
        res.pack()
        return
    res.pack_forget()
    try:
        res=Label(text=x/y)
    except:
        res.pack_forget()
        res = Label(text=('Ошибка'))
        res.pack()
        return
    res.pack()
main=Tk()
main.geometry('50x200')
bwidth=10
res=Label()
inp1=Entry(justify=CENTER)
inp2=Entry(justify=CENTER)
btn1=Button(text='+', width=bwidth, command=calcplus)
btn2=Button(text='-', width=bwidth, command=calcminus)
btn3=Button(text='*', width=bwidth, command=calcmn)
btn4=Button(text='/', width=bwidth, command=calcdel)
inp1.pack()
inp2.pack()
btn1.pack()
btn2.pack()
btn3.pack()
btn4.pack()
lbl1=Label(inp1)
main.mainloop()
