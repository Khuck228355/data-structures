class Student:
    def __init__(self, name, id):
        self.name = name
        self.id = id
        # self.get_name = self.name + ' obtained ' + self.id + ' id'
    @property
    def get_name(self):
        return self.name + ' - ' + self.id

st = Student("Ivan", '10')
print(st.name)
print(st.get_name)
print('---------')
st.name = "Oleg"
print(st.name)
print(st.get_name)
