print('Декораторы классов property')
class MyClass:
    def __init__(self, x):
        self.x = x

    @property
    def x_10(self):
        return self.x * 10

    @x_10.setter
    def x_10(self, x_10):
        self.x = x_10 // 2


my_object = MyClass(10)
print(my_object.x_10)  # 10
print(my_object.x)  # 5
my_object.x_10 = 7  #
print(my_object.x_10)  # 100
print(my_object.x)  # 50

print('Декоратор функции')
def decorator1(function):
    def wrapper():
        print('Это первое сообщение')
        function()
        print ('Это второе сообщение')
    return wrapper
def decorator2():
    print('Это теретье сообщение')
decorator2 = decorator1(decorator2)
decorator2()
