from collections import namedtuple
Student = namedtuple('Student', 'name age mark city')
students = (
   Student('Елена', '13', 7.1, 'Москва'),
   Student('Ольга', '11', 7.9, 'Иваново'),
   Student('Елизавета', '14', 9.1, 'Тверь'),
   Student('Дмитрий', '12', 5.2, 'Челябинск'),
   Student('Максим', '15', 6.1, 'Самара'),
   Student('Николай', '11', 8.7, 'Владивосток'),
   Student('Артур', '13', 5.8, 'Екатеринбург')
)


def good_students(students):
   total_mark = 0

   for student in students:
       total_mark += student.mark
   avg_mark = total_mark / len(students)

   good_mark_students = [student.name for student in students if student.mark >= avg_mark]
   print('Ученики ', ', '.join(good_mark_students), ' в этом семестре хорошо учатся!')

good_students(students)