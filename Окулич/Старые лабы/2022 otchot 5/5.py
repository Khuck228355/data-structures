class Perimetr:
    def get_pr(self):
        raise NotImplementedError('В дочерних классах должен быть метод "getpr"')
class Rectangle(Perimetr):
    def __init__(self, w, h):
        self. w = w
        self.h = h
class Square(Perimetr):
    def __init__(self, a):
        self.a = a
r1 = Rectangle(1, 2)
r2 = Rectangle(3, 4)
print("Периметры прямоугольников =", r1.get_pr(), "и", r2.get_pr())
s1 = Square(10)
s2 = Square(20)
print("Периметры квадратов =", s1.get_pr(), "и", s2.get_pr())
